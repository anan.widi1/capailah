import 'dart:ui';

class AppColors {
  static final biru = Color(0xFF6B7DB3);
  static final krem = Color(0xFFFF9999);
  static final coklat = Color(0xFFB36B6B);
}
