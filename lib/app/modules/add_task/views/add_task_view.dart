import 'dart:math';

import 'package:capailah/utils/app_color.dart';
import 'package:capailah/utils/widgets/button_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../utils/widgets/textfield_widget.dart';
import '../controllers/add_task_controller.dart';

class AddTaskView extends GetView<AddTaskController> {
  const AddTaskView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = new TextEditingController();
    TextEditingController deskController = new TextEditingController();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
        title: const Text("capailah",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14,
                color: Colors.black)),
      ),
      body: Container(
          padding: const EdgeInsets.all(20),
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover, image: AssetImage("assets/bg3.png"))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text("Nama :",
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                      color: Colors.black)),
              TextFieldWidget(
                  textController: nameController, hintText: "Masukan nama"),
              const SizedBox(
                height: 20,
              ),
              TextFieldWidget(
                textController: deskController,
                hintText: "Masukan deskripsi",
                maxLines: 3,
              ),
              const SizedBox(
                height: 20,
              ),
              ButtonWidget(
                  backgroundColor: AppColors.coklat,
                  text: "Tambah",
                  textColor: Colors.white),
            ],
          )),
    );
  }
}
