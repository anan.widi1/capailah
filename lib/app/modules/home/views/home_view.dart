import 'package:capailah/app/modules/alltask/views/alltask_view.dart';
import 'package:capailah/utils/app_color.dart';
import 'package:capailah/utils/widgets/button_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Container(
            alignment: Alignment.centerLeft,
            child: const Text("capailah",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Colors.black)),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(20),
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover, image: AssetImage("assets/bg.png"))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Text("Hai kamu !",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 32,
                    fontWeight: FontWeight.w600,
                  )),
              const Text("Atur Kegiatanmu Mulai dari Sekarang",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                  )),
              SizedBox(
                height: MediaQuery.of(context).size.height / 1.9,
              ),
              InkWell(
                onTap: () {
                  Get.to(AlltaskView(),
                      transition: Transition.fade,
                      duration: Duration(seconds: 1));
                },
                child: ButtonWidget(
                    backgroundColor: AppColors.biru,
                    text: "ATUR",
                    textColor: Colors.white),
              )
            ],
          ),
        ));
  }
}
