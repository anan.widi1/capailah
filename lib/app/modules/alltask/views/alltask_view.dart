import 'package:capailah/app/modules/add_task/views/add_task_view.dart';
import 'package:capailah/app/modules/home/views/home_view.dart';
import 'package:capailah/utils/app_color.dart';
import 'package:capailah/utils/widgets/button_widget.dart';
import 'package:capailah/utils/widgets/task_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/alltask_controller.dart';

class AlltaskView extends GetView<AlltaskController> {
  const AlltaskView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List myData = [
      "try harder",
      "try smarter",
      "be patient",
      "try harder",
      "try smarter",
      "be patient",
      "try harder",
      "try smarter",
      "be patient"
    ];

    final LeftEditCon = Container(
      color: AppColors.biru.withOpacity(0.7),
      margin: const EdgeInsets.only(bottom: 10),
      alignment: Alignment.centerLeft,
      child: const Icon(
        Icons.edit,
        color: Colors.white,
      ),
    );
    final RightDelCon = Container(
      color: Colors.redAccent,
      margin: const EdgeInsets.only(bottom: 10),
      alignment: Alignment.centerRight,
      child: const Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              )),
          title: const Text("capailah",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Colors.black)),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      Get.to(HomeView(),
                          transition: Transition.fade,
                          duration: Duration(seconds: 1));
                    },
                    child: Icon(
                      Icons.home,
                      color: AppColors.krem,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.5),
                      color: AppColors.krem,
                    ),
                    child: InkWell(
                      onTap: () {
                        Get.to(AddTaskView(),
                            transition: Transition.fade,
                            duration: Duration(seconds: 1));
                      },
                      child: const Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 20,
                      ),
                    ),
                  ),
                  Expanded(child: Container()),
                  Icon(
                    Icons.calendar_month_sharp,
                    color: AppColors.krem,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Text(
                    "2",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  )
                ],
              ),
            ),
            Container(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: const Text("Daftar Kegiatan Kamu :")),
            Flexible(
              child: ListView.builder(
                  itemCount: myData.length,
                  itemBuilder: (context, index) {
                    return Dismissible(
                      background: LeftEditCon,
                      secondaryBackground: RightDelCon,
                      onDismissed: (DismissDirection direction) {
                        print("after dismis");
                      },
                      confirmDismiss: (DismissDirection direction) async {
                        if (direction == DismissDirection.startToEnd) {
                          showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              barrierColor: Colors.transparent,
                              context: context,
                              builder: (_) {
                                return Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.krem.withOpacity(0.1),
                                      borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        topRight: Radius.circular(20),
                                      )),
                                  height: 550,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        ButtonWidget(
                                            backgroundColor: AppColors.krem,
                                            text: "VIEW",
                                            textColor: Colors.white),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        ButtonWidget(
                                            backgroundColor: AppColors.krem,
                                            text: "EDIT",
                                            textColor: Colors.white),
                                      ],
                                    ),
                                  ),
                                );
                              });
                        } else {
                          return Future.delayed(Duration(seconds: 1),
                              () => direction == DismissDirection.endToStart);
                        }
                      },
                      key: ObjectKey(index),
                      child: Container(
                        margin: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 10),
                        child: TaskWidget(
                            text: myData[index], color: Colors.blueGrey),
                      ),
                    );
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 200, right: 20, bottom: 20, top: 20),
              child: InkWell(
                onTap: () {
                  Get.to(AddTaskView());
                },
                child: ButtonWidget(
                    backgroundColor: AppColors.krem,
                    text: "Tambah",
                    textColor: Colors.white),
              ),
            )
          ],
        ));
  }
}
