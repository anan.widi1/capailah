import 'package:get/get.dart';

import '../controllers/alltask_controller.dart';

class AlltaskBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AlltaskController>(
      () => AlltaskController(),
    );
  }
}
